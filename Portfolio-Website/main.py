from flask import Flask, render_template
# Create a flask Instance
app = Flask(__name__)

# Create a route decorator
@app.route('/') 
def index():
    
    return render_template("index.html")

# Create a route decorator
@app.route('/about')
def about():
    return render_template("about.html")
  
# Create a route decorator
@app.route('/contact')
def contact():
    return render_template("contact.html")
  
# Create a route decorator
@app.route('/skills')
def skills():
    projects = [
        {'name': 'Portfolio', 'description': 'Project description 1', 'picture': 'Screenshot 1'},
        {'name': 'SoS game', 'description': 'Project description 2', 'picture': 'Screenshot 2'},
        {'name': 'Networking Lab', 'description': 'Project description 3', 'picture': 'Screenshot 3'},
    ]
    
    education_background = [
        {'class_code': 'COMP_SCI-404',
         'class_name': 'Introduction to Algorithms and Complexity', 
         'class_description': 'A rigorous review of asymptotic analysis techniques and \
         algorithms: from design strategy (such as greedy, divide-and-conquer, and dynamic \
         programming) to problem areas (such as searching, sorting, shortest path, spanning \
         trees, transitive closures, and other graph algorithms, string algorithms) arriving at \
         classical algorithms with supporting data structures for efficient implementation. '},
        
        {'class_code': 'COMP_SCI-470', 
         'class_name': 'Introduction to Database Management Systems', 
         'class_description': 'This course covers database architecture, data independence, \
         schema, Entity-Relationship (ER) and relational database modeling, relational algebra \
         and calculus, SQL, file organization, relational database design, physical database \
         organization, query processing and optimization, transaction structure and execution, \
         concurrency control mechanisms, database recovery, and database security. '},
        
        {'class_code': 'COMP_SCI-201', 
         'class_name': 'Problem Solving and Programming II', 
         'class_description':'Problem solving and programming using classes and objects.\
         Algorithm efficiency, abstract data types, searching and sorting, templates, pointers,\
         linked lists, stacks and queues implemented in C++. '},
        
        {'class_code': 'COMP_SCI-490WD', 
         'class_name': 'Web Development', 
         'class_description': 'Covers several technologies that allow creating dynamic web applications: \
         HTML, CSS, MySQL, JavaScript and a framework.'},
        
        {'class_code': 'COMP_SCI-431', 
         'class_name': 'Introduction to Operating Systems', 
         'class_description': 'This course covers concurrency and control of \
         asynchronous processes, deadlocks, memory management, processor and disk \
         scheduling, x86 assembly language, parallel processing, security, protection, \
         and file system organization in operating systems. '},
        
        {'class_code': 'COMP_SCI-281R', 
         'class_name': 'Introduction to Computer Architecture and Organization', 
         'class_description': 'Digital Logic and Data Representation, process architecture and \
         instruction sequencing, memory hierarchy and bus-interfaces and functional organization. '},
        
        {'class_code': 'COMP_SCI-449', 
         'class_name': 'Foundations of Software Engineering', 
         'class_description': 'An introduction to the fundamental principles and practices of software engineering.'},
        
        {'class_code': 'COMP_SCI-394R', 
         'class_name': 'Applied Probability', 
         'class_description': 'Basic concepts of probability theory.'},
    ]
    return render_template("skills.html", projects=projects, education_background=education_background)


app.run(host='0.0.0.0', port=81)

